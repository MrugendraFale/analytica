'''*** 
Author : Mrugendra Fale  
Date : 21/08/2019 1:06:AM

***'''

import numpy as np

from bokeh.plotting import figure, output_file, show
from bokeh.sampledata.stocks import MSFT

# prepare some data
msft = np.array(MSFT['adj_close'])
msft_dates = np.array(MSFT['date'], dtype=np.datetime64)

window_size = 30
window = np.ones(window_size)/float(window_size)
msft_avg = np.convolve(msft, window, 'same')

# output to static HTML file
output_file("afterlog.component.html", title="Microsoft stocks Revenue")

# create a new plot with a datetime axis type
p = figure(plot_width=1300, plot_height=768, x_axis_type="datetime")

# add renderers
p.circle_x(msft_dates, msft, size=4, color='darkgrey', alpha=0.2, legend_label='close')
p.line(msft_dates, msft_avg, color='darkred', legend_label='avg')

# NEW: customize by setting attributes

p.title.text = "MSFT One-Month Average"
p.legend.location = "top_right"
p.grid.grid_line_alpha = 0
p.xaxis.axis_label = 'Year'
p.yaxis.axis_label = 'Price'
p.xgrid.band_fill_color = "gray"
p.xgrid.band_fill_alpha = 0.1

# show the results
show(p)
